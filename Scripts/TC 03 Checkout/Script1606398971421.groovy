import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.callTestCase(findTestCase('TC 01 Add to chart'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.tap(findTestObject('android.widget.TextView1'), 0)

Mobile.tap(findTestObject('android.widget.Button0 - CHECKOUT'), 0)

Mobile.setText(findTestObject('android.widget.EditText0 - Your Name'), 'Citra', 0)

Mobile.setText(findTestObject('android.widget.EditText1 - your.emailgmail.com'), 'citra.sekar.konsultan@work.bri.co.id', 
    0)

Mobile.setText(findTestObject('android.widget.EditText2 - 628123456789'), '085213486472', 0)

Mobile.setText(findTestObject('android.widget.EditText3 - Your Address'), 'Perum Bumi Pertiwi 1 Jl Banda', 0)

Mobile.tap(findTestObject('android.widget.LinearLayout8'), 0)

Mobile.tap(findTestObject('android.widget.CheckedTextView1 - Cash On Delivery'), 0)

Mobile.setText(findTestObject('android.widget.EditText5 - Comment'), 'Send ASAP', 0)

Mobile.tap(findTestObject('android.widget.Button0 - PROCESS CHECKOUT'), 0)

Mobile.tap(findTestObject('android.widget.Button1 - YES'), 0)

Mobile.tap(findTestObject('android.widget.Button0 - OK'), 0)

